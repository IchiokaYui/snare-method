# coding: UTF-8
import numpy as np
from scipy.io.wavfile import read
from scipy.io.wavfile import write
import sys

def all_write(path, samp_rate, left, right, print_state=True):
    write(path, samp_rate, np.c_[left, right])
    if print_state:
        print("complete")

if __name__ == "__main__":
    np.random.seed(114514)
    rand = np.random.uniform(-1, 1, size=44100*10) * 2147483647
    left = np.array(rand, dtype=np.int32)
    right = np.array(np.zeros(len(left)), dtype=np.int32)
    all_write("white.wav", 44100, left, right)
