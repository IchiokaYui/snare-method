# 透かし入りパーカッション音声を用いたデータハイディング手法
パーカッションの1つであるスネアの音声を透かし情報から生成し，楽曲に楽器の一部として埋め込むデータハイディング方式の提案と実装を行った.

# 各ファイルについて
|ファイル|概要|
|:---|:---|
|README.md|このファイル(説明ファイル)|
|snare_method.py|透かし入りパーカッション音声を用いたデータハイディング手法の実装|
|make_white_noise.py|ホワイトノイズを生成するだけのスクリプト|

# 設定
実験にはUbuntu 14.04(64bit)を用いた.
SciPyとかNumPyを使えるようにしておく.

## SciPyのインストール
http://stackoverflow.com/questions/2213551/installing-scipy-with-pip

```
sudo apt-get install build-essential gfortran libatlas-base-dev python-pip python-dev
sudo pip install --upgrade pip
sudo pip install scipy
```