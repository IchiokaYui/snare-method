# coding: UTF-8

# 卒論の付録の内容の実装です
# このプログラムは無駄なことをわりとしてます
# 付録に書き起こした数式の方がシンプルです
# ちゃんと動きます
# もし使う方がいたら参考程度に
# パラメータは付録の内容と一緒です

import numpy as np
from scipy.io.wavfile import read
from scipy.io.wavfile import write
import scipy.signal as sig
import sys
import matplotlib.pyplot as plt
import random
import functools
import os
import psutil
import math

# info1とinfo2とのハミング距離を返す
# https://ja.wikipedia.org/wiki/%E3%83%8F%E3%83%9F%E3%83%B3%E3%82%B0%E8%B7%9D%E9%9B%A2
# もっと早くできる
# 非破壊的メソッド
def hamming_dist(info1, info2):
    dist = 0
    for i in range(len(info1)):
        if info1[i] != info2[i]:
            dist += 1
    return dist

def pn_series(length, seed=None):
    if seed != None:
        np.random.seed(seed)
    return np.random.uniform(-1, 1, size=length)

def bin_pn_series(length, seed=None):
    if seed != None:
        np.random.seed(seed)
    return np.random.randint(2, size=length)

def amp_drum(length, seed, vol=10000, alpha=(1/1.0003), dtype=None):
    base_drum = pn_series(length=10000, seed=seed) * float(vol)
    for idx, val in enumerate(base_drum):
        base_drum[idx] = (alpha ** idx) * val

    if dtype == None:
        return base_drum
    else:
        return np.array(base_drum, dtype=dtype)

def amp_drums(num, length, seed, vol=10000, alpha=(1/1.0003), dtype=None):
    drums = []
    np.random.seed(seed)
    seeds = np.random.randint(0, 1000000000, num)
    for i in range(num):
        drum = amp_drum(length, seeds[i], vol, alpha, dtype)
        drums.append(drum)
    return drums

# samp_rate, left, right, dtype = all_read("input.wav")
def all_read(path, print_state=True):
    samp_rate, data = read(path)
    if print_state:
        print("sampling rate:", samp_rate)
        print("sample num:", len(data[:, 0]))
        print("channel:", data.shape[1])
        print("dtype:", data.dtype)
    if data.shape[1] != 2:
        print("not have 2 channels")
        sys.exit()
    return samp_rate, data[:, 0], data[:, 1], data.dtype

def all_write(path, samp_rate, left, right, print_state=True):
    write(path, samp_rate, np.c_[left, right])
    if print_state:
        print("complete")

# start分の数を0を先頭に配列aにつけ、配列長がadj_lenとなるようにさらに後ろに0をつける
# adj_lenを超えるような場合はFalseを返す
def len_adjust(a, adj_len, start, dtype=None):
    end_zero_num = adj_len - ( len(a) + start )
    if end_zero_num < 0:
        return False
    tmp = np.r_[np.zeros(start), a]
    tmp = np.r_[tmp, np.zeros(end_zero_num)]
    if dtype == None:
        return tmp
    else:
        return np.array(tmp, dtype=dtype)

# 拍と拍の間の時間
# 秒単位で返す
def rhythm_interval(bpm):
    return 1.0/(bpm/60.0)

# 0と1で構成される配列を2進数と見立てて整数を返す
# [0, 0, 1, 1]なら3を返す
def bin_array_to_num(a):
    return int(",".join(map(str,a)).replace(",", ""), 2)

# 整数を0,1からなる文字列を返す
# n=3ならdigit=4なら0011を返す
def num_to_bin_str(n, digit):
    return format(n, 'b').zfill(digit)

# 0と1からなる文字列を整数の0と1からなる配列を返す
# 0011なら[0, 0, 1, 1]を返す
def bin_str_to_array(s):
    return list(map(int, list(s)))

# 整数からなる配列を0と1からなる配列に変換して返す
# [4, 3, 6]でdigit=4だったら[0,1,0,0, 0,0,1,1, 0,1,1,0]を返す
def num_to_bin_array(a, digit):
    str_a = list(map(functools.partial(num_to_bin_str, digit=digit), a))
    return bin_str_to_array("".join(str_a))

# [[1 2 3 6 5 6 7 8]
#  [8 2 3 4 9 6 7 8]
#  [1 2 3 4 5 6 7 9]]
# [1, 1, 2]
# [[1 2 3 6 5 6 7 8]
#  [8 2 3 4 9 6 7 8]
#  [1 2 3 4 5 6 7 9]]
# [1, 0, 1, 2]
def block_ranks(a, scan_width, window=None):
    a_width = len(a[0])
    ret = [None] * math.ceil(float(a_width)/scan_width)
    for i in range(len(ret)):
        block = a[:,i*scan_width:(i+1)*scan_width]
        if window is not None:
            block *= window
        block_width = len(block[0])
        top_idx = block.argmax()
        ret[i] = int( float(top_idx) / block_width )
    return ret

def first_shift_from_block(a, scan_width):
    block = a[:,0:scan_width]
    top_idx = block.argmax()
    return top_idx % scan_width

def one_win(full_width, start_pt, side_width):
    win = [1]*(side_width*2+1)
    win.extend([0]*(full_width-len(win)))
    win.extend(win)
    win = win[full_width-start_pt+side_width:2*full_width-start_pt+side_width]
    return win

if __name__ == "__main__":
    start_samps = int(1.416*44100)
    bpm = 143
    beats_num = 32 # 曲中のドラムがなる数
    bpb = 8 # bit per beat
    sec_info = bin_pn_series(bpb * beats_num, seed=810114514)
    beat_info = [sec_info[i:i+bpb] for i in range(0,len(sec_info), bpb)]


    # # 埋め込み(多重化)
    # samp_rate, left, right, dtype = all_read("RYDEEN_15s.wav")
    # drums = amp_drums(2**bpb, 100000, 123456, dtype=dtype, vol=1500)
    # for pointer in range(beats_num):
    #     # 埋め込む情報に対応する音
    #     drum_idx = bin_array_to_num(beat_info[pointer])
    #     d = drums[drum_idx]
    #     # 書き込み等
    #     add_amp = len_adjust(d, len(left), int(start_samps+pointer*rhythm_interval(bpm)*samp_rate), dtype=dtype)
    #     left += add_amp
    #     pointer += 1
    # all_write("out.wav", samp_rate, left, right)

    # 抽出(多重化)
    samp_rate, left, right, dtype = all_read("150cm.wav")
    drums = amp_drums(2**bpb, 100000, 123456, dtype=dtype, vol=1500)
    corres = []
    for pointer in range(2 ** bpb):
        print("corre:", pointer)
        # corre = sig.correlate(np.array(left, dtype=np.float64), drums[pointer], mode="full", method="auto")
        corre = sig.correlate(np.array(left, dtype=np.float32), drums[pointer], mode="full", method="auto")
        corres.append(corre)

    # 一時的に抽出
    print("一時的に抽出")
    corres = np.array(corres)
    scan_w = int(rhythm_interval(bpm)*samp_rate)
    extra_info = block_ranks(corres, scan_w)
    # print("scan_w", scan_w)

    # !同期取りが雑
    print(extra_info)
    # tmp_idx = extra_info.index(54)

    # 同期情報から必要なだけ切り抜く
    print("同期情報から必要なだけ切り抜く")
    new_corres = np.array(corres)
    new_corres = new_corres[:,tmp_idx*scan_w:tmp_idx*scan_w+beats_num*scan_w]
    # print(new_corres.shape)

    # 最初の信号から窓を作成
    print("最初の信号から窓を作成")
    fisrt_shift = first_shift_from_block(new_corres, scan_w)
    row_win = one_win(scan_w, fisrt_shift, 20)
    block_win = np.reshape(np.array(row_win * (2 ** bpb)), (2 ** bpb, len(row_win)))
    # print(block_win.shape)

    # 窓をかけて改めて抽出
    print("窓をかけて改めて抽出")
    new_extra_info = block_ranks(new_corres, scan_w, window=block_win)
    print(new_extra_info)
    print(list(map(bin_array_to_num, beat_info)))
    new_extra_info = num_to_bin_array(new_extra_info, bpb)
    print("BER:", hamming_dist(new_extra_info, sec_info) / float(len(sec_info)))

    # plt.figure()
    # plt.plot(sum_corres)
    # plt.savefig("tmp.png")
